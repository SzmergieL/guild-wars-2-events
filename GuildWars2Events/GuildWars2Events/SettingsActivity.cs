using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Preferences;
using Java.Lang;
using System.Threading;


namespace GuildWars2Events
{
	[Activity (Label = "Settings")]			
	public class SettingsActivity : Activity
	{
		ISharedPreferences preferences;
		ISharedPreferencesEditor editor;
		MyDatabase mdTemp;
		Button syncButton;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			preferences = PreferenceManager.GetDefaultSharedPreferences (Application.Context);
			SetContentView (Resource.Layout.Settings);

			mdTemp = new MyDatabase("events_db");

			var worldSelect = FindViewById<Spinner> (Resource.Id.WorldSelect);
			var langSelect = FindViewById<Spinner> (Resource.Id.LangSelect);
			syncButton = FindViewById<Button> (Resource.Id.syncButton);

			var LangAdapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.lang_array, Android.Resource.Layout.SimpleSpinnerItem);
			LangAdapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			langSelect.Adapter = LangAdapter;

			var WorldNameAdapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.world_name_array, Android.Resource.Layout.SimpleSpinnerItem);
			WorldNameAdapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			worldSelect.Adapter = WorldNameAdapter;

			var WorldIdArray = Application.Resources.GetStringArray(Resource.Array.world_id_array);
			for (int i = 0; i < WorldIdArray.Count (); i++) {
				if (WorldIdArray [i] == preferences.GetInt ("world_id", 2204).ToString()) {
					worldSelect.SetSelection (i);
				}
			}

			var LangArray = Application.Resources.GetStringArray(Resource.Array.lang_array);
			for (int i = 0; i < LangArray.Count (); i++) {
				if (LangArray [i] == preferences.GetString ("lang", "en")) {
					langSelect.SetSelection (i);
				}
			}

			worldSelect.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (worldSelect_ItemSelected);
			langSelect.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (langSelect_ItemSelected);

			syncButton.Click += delegate {
//				mdTemp.SyncDatabase ("events_db");
				string info = string.Format ("Database syncing. Please wait a minute.");
				syncButton.Text = info;

				ThreadPool.QueueUserWorkItem (o => DatabaseLoader());
//				toast = string.Format ("Sync Done.");
//				Toast.MakeText (this, toast, ToastLength.Long).Show ();
			};
		}

		private void DatabaseLoader()
		{
			var request = new EventRequest (preferences.GetInt ("world_id", 2204), "", 0, preferences.GetString ("lang", "en"));
			mdTemp.SyncDatabase ("events_db");
			request.loadEventIntoDatabase (mdTemp);
			request.loadMapNameIntoDatabase (mdTemp);
			RunOnUiThread (() => syncButton.Text = "Sync done.");
		}


		private void worldSelect_ItemSelected (object sender, AdapterView.ItemSelectedEventArgs e)
		{
			Spinner spinner = (Spinner)sender;

			var WorldIdArray = Application.Resources.GetStringArray(Resource.Array.world_id_array);
			editor = preferences.Edit ();
			editor.PutInt ("world_id", Integer.ParseInt(WorldIdArray [spinner.SelectedItemPosition]));
			editor.Commit ();
		}

		private void langSelect_ItemSelected (object sender, AdapterView.ItemSelectedEventArgs e)
		{
			Spinner spinner = (Spinner)sender;

			var LangArray = Application.Resources.GetStringArray(Resource.Array.lang_array);
			editor = preferences.Edit ();
			editor.PutString ("lang", LangArray[spinner.SelectedItemPosition]);
			editor.Commit ();
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			var MenuItem1 = menu.Add ("Favourite Events");
			var MenuItem2 = menu.Add ("Filter Events");

			return base.OnCreateOptionsMenu (menu);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.TitleFormatted.ToString()) {
			case "Favourite Events":
				StartActivity (typeof(FavListActivity));
				break;
			case "Filter Events":
				StartActivity (typeof(FilterActivity));
				break;
			}
			return base.OnOptionsItemSelected (item);
		}

	}
}