using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Preferences;
using System.Threading;

namespace GuildWars2Events
{
	[Activity (Label = "Guild Wars 2 Events", MainLauncher = true)]			
	public class FavListActivity : Activity
	{
		ImageView refreshButton;
		ListView FavListViewLV;
		MyDatabase db;
		EventRequest req;
		ISharedPreferences pref;
		SimpleCursorAdapter scaTemp;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.FavListView);
			db = new MyDatabase ("events_db");
			pref = PreferenceManager.GetDefaultSharedPreferences (Application.Context);
			req = new EventRequest (pref.GetInt ("world_id", 2204), "", 0, pref.GetString ("lang", "en"));

			refreshButton = FindViewById<ImageView> (Resource.Id.filterButton);
			FavListViewLV = FindViewById <ListView> (Resource.Id.FavListViewLV);
			FavListViewLV.ItemClick += new EventHandler<AdapterView.ItemClickEventArgs> (FavListViewLV_ItemClick);

			refreshButton.Click += delegate {
				ThreadPool.QueueUserWorkItem(o => updateState() );
//				GetFavListCursorView();
			};

			GetFavListCursorView();
		}
		
		private void GetFavListCursorView ()
		{
			Android.Database.ICursor icTemp = db.GetFavListCursor ();
			if (icTemp != null) {
				icTemp.MoveToFirst ();
				string[] from = new string[] {"name","mapName","level", "state" };
				int[] to = new int[] {
					Resource.Id.FLVname,
					Resource.Id.FLVmapName,
					Resource.Id.FLVlevel,
					Resource.Id.FLVstate,
				};
				scaTemp = new SimpleCursorAdapter (this, Resource.Layout.FavListItemView, icTemp, from, to);
				FavListViewLV.Adapter = scaTemp;
			} else {
//				Toast.MakeText (this, "Favourite List is empty! Add some events to favourite list", ToastLength.Long).Show ();		
			}
			if (FavListViewLV.Adapter.IsEmpty) {
				AlertDialog.Builder ad = new AlertDialog.Builder (this);
				ad.SetTitle ("Favourite List is empty");
				ad.SetMessage ("Add some events to favourites in EventFilter");
				ad.SetPositiveButton ("EventFilter", delegate {
					StartActivity (typeof(FilterActivity));
				});
				ad.SetNegativeButton ("Ignore", delegate {
				});
				ad.Show ();
//				Toast.MakeText (this, "Favourite List is empty! Add some events to favourite list", ToastLength.Long).Show ();		
			}
		}

		private void updateState ()
		{
			req.updateEventState (db);
//			scaTemp.NotifyDataSetChanged;
			RunOnUiThread (() => GetFavListCursorView () );
		}

		void FavListViewLV_ItemClick (object sender, AdapterView.ItemClickEventArgs e)
		{
			TextView name = e.View.FindViewById<TextView> (Resource.Id.FLVname);

			AlertDialog.Builder ad = new AlertDialog.Builder (this);
			ad.SetTitle ("Delete Event");
			ad.SetMessage ("Are you sure?");
			ad.SetPositiveButton ("Yes", delegate {
				db.RemoveFavRecord (name.Text.ToString());
				GetFavListCursorView();
			});
			ad.SetNegativeButton ("No", delegate {
			});
			ad.Show ();
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			var MenuItem1 = menu.Add ("Filter Events");
			var MenuItem2 = menu.Add ("Settings");

			return base.OnCreateOptionsMenu (menu);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.TitleFormatted.ToString()) {
			case "Filter Events":
				StartActivity (typeof(FilterActivity));
				break;
			case "Settings":
				StartActivity (typeof(SettingsActivity));
				break;
			}
			return base.OnOptionsItemSelected (item);
		}
	}
}

