using System;
using Android.Widget;
using Android.App;
using System.Collections.Generic;
using Android.Views;
using System.Linq;
using System.Net;
using System.IO;
using System.Json;
using Android.Content;
using Android.Preferences;

namespace GuildWars2Events
{
	public class EventListAdapter : BaseAdapter
	{
		Activity context;
		public List<GW2Event> items;
		MyDatabase db = new MyDatabase ("events_db");

		public EventListAdapter (Activity context) : base ()
		{
			this.context = context;
		}

		public override int Count
		{
			get { return items.Count; }
		}

		public override Java.Lang.Object GetItem(int position)
		{
			return position;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var item = items[position];

			var view = (convertView ??
				context.LayoutInflater.Inflate(
					Resource.Layout.EventListItem,
					parent,
					false)) as LinearLayout;


			var name = view.FindViewById (Resource.Id.name) as TextView;
			var mapName = view.FindViewById (Resource.Id.mapName) as TextView;
			var level = view.FindViewById (Resource.Id.level) as TextView;
			var state = view.FindViewById (Resource.Id.state) as TextView;

			name.Text = item.name;
			mapName.Text = item.mapName.ToString();
			level.Text = item.level.ToString();
			state.Text = item.state;

			return view;
		}


		public GW2Event GetItemAtPosition(int position)
		{
			return items[position];
		}
	}
}

