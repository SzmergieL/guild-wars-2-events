using System;
using System.Net;
using System.IO;
using System.Json;
using System.Collections.Generic;
using Android.Database;
using Android.Content;
using Android.Preferences;
using Android.App;

namespace GuildWars2Events
{
	public class EventRequest
	{
		public int world_id { get; set; }
		public int map_id { get; set; }
		public string event_id { get; set; }
		public string lang { get; set; }

		public EventRequest ( int world_id,  string event_id = "", int map_id = 0, string lang = "en"  )
		{
			this.world_id = world_id;
			this.map_id = map_id;
			this.event_id = event_id;
			this.lang = lang;
		}

		public void loadEventIntoDatabase (MyDatabase db)
		{
			var request2 = HttpWebRequest.Create (string.Format (@"https://api.guildwars2.com/v1/event_details.json?lang={0}",lang));
			request2.ContentType = "application/json";
			request2.Method = "GET";

			HttpWebResponse response2 = request2.GetResponse () as HttpWebResponse;
			StreamReader reader2 = new StreamReader (response2.GetResponseStream ());
				var content2 = reader2.ReadToEnd ();
				var Obj2 = JsonObject.Parse (content2);
				var properties2 = Obj2 ["events"];

			var request = HttpWebRequest.Create (string.Format (@"https://api.guildwars2.com/v1/events.json?world_id={0}&map_id={1}&event_id={2}",
				              world_id, map_id, event_id));
			request.ContentType = "application/json";
			request.Method = "GET";

			db.sqldTemp.BeginTransaction ();
			try {
				using (HttpWebResponse response = request.GetResponse () as HttpWebResponse) {
					using (StreamReader reader = new StreamReader (response.GetResponseStream ())) {
						var content = reader.ReadToEnd ();
						var Obj = JsonObject.Parse (content);
						var properties = Obj ["events"];

						for (int i = 0; i < properties.Count; i++) {
							string eventID = properties [i] ["event_id"];
							db.AddEventRecord (properties [i] ["world_id"], properties [i] ["map_id"], properties [i] ["event_id"]);
							db.AddEventNameLevelRecord(eventID,properties2[eventID]["name"],properties2[eventID]["level"]);
						}

					}
				}
				db.sqldTemp.SetTransactionSuccessful();
			}
			catch ( SQLException ex ) {
				db.Message = ex.ToString ();
			}
			finally {
				db.sqldTemp.EndTransaction ();
			}
		}

		public void updateEventState (MyDatabase db)
		{
			ISharedPreferences preferences = PreferenceManager.GetDefaultSharedPreferences (Application.Context);
			var request = HttpWebRequest.Create (string.Format (@"https://api.guildwars2.com/v1/events.json?world_id={0}&lang={1}",
				preferences.GetInt("world_id",2204),preferences.GetString("lang","en")));
			request.ContentType = "application/json";
			request.Method = "GET";

			List <string> eventids = new List<string>();
			ICursor cursor = db.GetFavListCursor ();
			if ( cursor.MoveToFirst () ) {
				do {
					eventids.Add ( cursor.GetString(1) );
				} while (cursor.MoveToNext());
			}
			cursor.Close();

			db.sqldTemp.BeginTransaction ();
			try
			{
				using (HttpWebResponse response = request.GetResponse () as HttpWebResponse) {
					using (StreamReader reader = new StreamReader (response.GetResponseStream ())) {
						var content = reader.ReadToEnd ();
						var Obj = JsonObject.Parse (content);
						var properties = Obj["events"];
						for ( int i = 0 ; i < properties.Count ; i++)
						{
							if ( eventids.Contains (properties [i] ["event_id"]) )
								{
									db.UpdateFavStatus(properties [i] ["event_id"],properties [i] ["state"]);
								}
						}
					}
				}
				db.sqldTemp.SetTransactionSuccessful();
			}
			catch ( SQLException ex ) {
				db.Message = ex.ToString ();
			}
			finally {
				db.sqldTemp.EndTransaction ();
			}
		}

		public void loadMapNameIntoDatabase (MyDatabase db)
		{
			var request = HttpWebRequest.Create (string.Format (@"https://api.guildwars2.com/v1/map_names.json?lang={0}",lang));
			request.ContentType = "application/json";
			request.Method = "GET";

			db.sqldTemp.BeginTransaction ();
			try {
				using (HttpWebResponse response = request.GetResponse () as HttpWebResponse) {
					using (StreamReader reader = new StreamReader (response.GetResponseStream ())) {
						var content = reader.ReadToEnd ();
						var Obj = JsonObject.Parse (content);
						var properties = Obj;

						for (int i = 0; i < properties.Count; i++) {
							db.AddMapNameRecord (properties [i]["id"], properties [i]["name"]);
						}
					}
				}
				db.sqldTemp.SetTransactionSuccessful();
			}
			catch ( SQLException ex ) {
				db.Message = ex.ToString ();
			}
			finally {
				db.sqldTemp.EndTransaction ();
			}
		}	
	}
}

