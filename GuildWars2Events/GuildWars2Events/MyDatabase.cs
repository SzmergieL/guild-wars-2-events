using System;
using Android.Database.Sqlite;
using System.IO;

namespace GuildWars2Events
{
	public class MyDatabase
	{
		public SQLiteDatabase sqldTemp;
		private string sSQLQuery;
		private string sMessage;
		private  bool bDBIsAvailable;

		public MyDatabase ()
		{
			Message = "";
			bDBIsAvailable = false;
		}

		public MyDatabase ( string DatabaseName )
		{
			try
			{
				sMessage = "" ;
				bDBIsAvailable = false;
				CreateDatabase ( DatabaseName );
			}
			catch (SQLiteException ex )
			{
				sMessage = ex.Message;
			}
		}

		public bool DatabaseAvailabe 
		{
			get { return bDBIsAvailable; }
			set { bDBIsAvailable = value; }
		}

		public string Message 
		{
			get{ return sMessage;}
			set{ sMessage = value;}
		}

		public void CreateDatabase (string sDatabaseName)
		{
			try {
				sMessage = "";
				string sLocation = 
					System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
				string sDB = Path.Combine (sLocation, sDatabaseName);
				bool bIsExists = File.Exists (sDB); 
				       
				if (!bIsExists) {
					sqldTemp = SQLiteDatabase.OpenOrCreateDatabase (sDB, null);

					sSQLQuery = "CREATE TABLE IF NOT EXISTS " +
                                "FavList " +
					            "(_id INTEGER PRIMARY KEY AUTOINCREMENT, event_id VARCHAR UNIQUE, name VARCHAR, mapName VARCHAR, level INT, state VARCHAR);";
					sqldTemp.ExecSQL (sSQLQuery);

					// event table
					sSQLQuery = "CREATE TABLE IF NOT EXISTS " +
					            "EventList " +
					            "(_id INTEGER PRIMARY KEY AUTOINCREMENT,world_id INT,map_id INT,event_id VARCHAR);";
					sqldTemp.ExecSQL (sSQLQuery);

					// eventnamelevel table
					sSQLQuery = "CREATE TABLE IF NOT EXISTS " +
					            "EventNameLevel " +
					            "(_id INTEGER PRIMARY KEY AUTOINCREMENT,event_id VARCHAR,name VARCHAR,level INT);";
					sqldTemp.ExecSQL (sSQLQuery);

					// map name table
					sSQLQuery = "CREATE TABLE IF NOT EXISTS " +
					            "MapName " +
					            "(_id INTEGER PRIMARY KEY AUTOINCREMENT, map_id INT, mapName VARCHAR);";
					sqldTemp.ExecSQL (sSQLQuery);

					sMessage = "New database is created.";
				} else {
					sqldTemp = SQLiteDatabase.OpenDatabase (sDB, null, DatabaseOpenFlags.OpenReadwrite);
					sMessage = "Database is opened.";
				}
				bDBIsAvailable = true;
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
		}

		public void SyncDatabase (string sDatabaseName )
		{
			try {
				sMessage = "";
				string sLocation = 
					System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
				string sDB = Path.Combine (sLocation, sDatabaseName);
				bool bIsExists = File.Exists (sDB); 

				if (bIsExists) {
					sqldTemp = SQLiteDatabase.OpenOrCreateDatabase (sDB, null);

					// event table
					sSQLQuery = "DROP TABLE IF EXISTS EventList;";
					sqldTemp.ExecSQL (sSQLQuery);

					sSQLQuery = "CREATE TABLE " +
					            "EventList " +
					            "(_id INTEGER PRIMARY KEY AUTOINCREMENT,world_id INT,map_id INT,event_id VARCHAR);";
					sqldTemp.ExecSQL (sSQLQuery);

					// eventnamelevel table
					sSQLQuery = "DROP TABLE IF EXISTS EventNameLevel;";
					sqldTemp.ExecSQL (sSQLQuery);
					sSQLQuery = "CREATE TABLE " +
					            "EventNameLevel " +
					            "(_id INTEGER PRIMARY KEY AUTOINCREMENT,event_id VARCHAR,name VARCHAR,level INT);";
					sqldTemp.ExecSQL (sSQLQuery);

					// map name table
					sSQLQuery = "DROP TABLE IF EXISTS MapName;";
					sqldTemp.ExecSQL (sSQLQuery);
					sSQLQuery = "CREATE TABLE " +
					            "MapName " +
					            "(_id INTEGER PRIMARY KEY AUTOINCREMENT, map_id INT, mapName VARCHAR);";
					sqldTemp.ExecSQL (sSQLQuery);

					sSQLQuery = "DROP TABLE IF EXISTS FavList;";
					sqldTemp.ExecSQL (sSQLQuery);
					sSQLQuery = "CREATE TABLE IF NOT EXISTS " +
                                "FavList " +
					            "(_id INTEGER PRIMARY KEY AUTOINCREMENT, event_id VARCHAR UNIQUE, name VARCHAR, mapName VARCHAR, level INT, state VARCHAR);";
					sqldTemp.ExecSQL (sSQLQuery);

					sMessage = "New database is created.";
				} else {
//					CreateDatabase(sDatabaseName);
//					sqldTemp = SQLiteDatabase.OpenDatabase (sDB, null, DatabaseOpenFlags.OpenReadwrite);
//					sMessage = "Database is opened.";
				}
				bDBIsAvailable = true;
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
		}

		public void AddEventRecord (int world_id, int map_id, string event_id)
		{
			try {
				sSQLQuery = "INSERT INTO " +
				            "EventList " +
				            "(world_id, map_id, event_id) " +
				            "VALUES(" + world_id + "," + map_id + ",'" + event_id + "');";
				sqldTemp.ExecSQL (sSQLQuery);
				sMessage = "Record is saved.";
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
		}

		public void AddEventNameLevelRecord (string event_id, string name, int level)
		{
			try {
				sSQLQuery = "INSERT INTO " +
				            "EventNameLevel " +
				            "(event_id, name, level) " +
				            "VALUES('" + event_id + "','" + name.Replace("'","''") + "'," + level + ");";
				sqldTemp.ExecSQL (sSQLQuery);
				sMessage = "Record is saved.";
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
		}

		public void AddWorldNameRecord (int world_id, string name)
		{
			try {
				sSQLQuery = "INSERT INTO " +
				            "WorldName " +
				            "(world_id, name) " +
				            "VALUES(" + world_id + ",'" + name.Replace("'","''") + "');";
				sqldTemp.ExecSQL (sSQLQuery);
				sMessage = "Record is saved.";
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
		}

		public string getEventId ( string name )
		{
			Android.Database.ICursor icTemp = null;
			string sSQLQuery;
			string sMessage = "";
			try {
				sSQLQuery = "SELECT event_id FROM EventNameLevel WHERE EventNameLevel.name = '" + name.ToString().Replace("'","''") + "';";

				icTemp = sqldTemp.RawQuery (sSQLQuery, null);
				if (!(icTemp != null)) {
					sMessage = "Record not found.";
				}
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
			icTemp.MoveToFirst ();
			return icTemp.GetString (0);
		}

		public void AddFavRecord (string event_id, string name, string mapName, int level, string state)
		{
			try {
				sSQLQuery = "INSERT OR IGNORE INTO " +
				            "FavList " +
				            "(event_id, name, mapName, level, state) " +
				            @"VALUES('" + event_id + "','" + name.Replace("'","''") + "','" + mapName.Replace("'","''") + "'," + level + ",'" + state + "');";
				sqldTemp.ExecSQL (sSQLQuery);
				sMessage = "Event added to favourites.";
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
		}

		public void RemoveFavRecord (string name)
		{
			try {
				sSQLQuery = "DELETE FROM " +
				            "FavList " +
				            "WHERE name = '" + name.ToString().Replace("'","''") + "';";
				sqldTemp.ExecSQL (sSQLQuery);
				sMessage = "Event removed from favourites.";
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
		}

		public void UpdateFavStatus (string event_id, string state)
		{
			try {
				sSQLQuery = "UPDATE " +
				            "FavList " +
				            "SET state='" + state + "' WHERE event_id = '" + event_id + "';";
				sqldTemp.ExecSQL (sSQLQuery);
				sMessage = "Event status updated.";
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
		}

		public void AddMapNameRecord (int map_id, string mapName)
		{
			try {
				sSQLQuery = "INSERT INTO " +
				            "MapName " +
				            "(map_id, mapName) " +
				            @"VALUES(" + map_id + ",'" + mapName.Replace("'","''") + "');";
				sqldTemp.ExecSQL (sSQLQuery);
				sMessage = "Record is saved.";
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
		}

		public Android.Database.ICursor GetFavListCursor()
		{
			Android.Database.ICursor icTemp = null;
			try {
				sSQLQuery = "SELECT _id, event_id, name, mapName, level, state FROM FavList;";

				icTemp = sqldTemp.RawQuery (sSQLQuery, null);
				if (!(icTemp != null)) {
					sMessage = "Record not found.";
				}
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
			return icTemp;
		}

		public Android.Database.ICursor GetRecordCursor ()
		{
			Android.Database.ICursor icTemp = null;
			try {
//				sSQLQuery = "SELECT * FROM (SELECT * FROM EventList JOIN MapName " +
//				            "ON EventList.map_id = MapName.map_id) AS J2 JOIN EventNameLevel " +
//				            "ON J2.event_id = EventNameLevel.event_id";
				sSQLQuery = "SELECT _id, name, mapName, level FROM (SELECT event_id, mapName FROM EventList JOIN MapName " +
				            "ON EventList.map_id = MapName.map_id) AS J2 JOIN EventNameLevel " +
				            "ON J2.event_id = EventNameLevel.event_id";

				icTemp = sqldTemp.RawQuery (sSQLQuery, null);
				if (!(icTemp != null)) {
					sMessage = "Record not found.";
				}
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
			return icTemp;
		}

		public Android.Database.ICursor GetRecordCursor (string sColumn, string sValue)
		{
			Android.Database.ICursor icTemp = null;
			try {
				sSQLQuery = "SELECT _id, name, mapName, level FROM ((SELECT event_id, mapName FROM EventList JOIN MapName " +			         
				            "ON EventList.map_id = MapName.map_id) AS J2 JOIN EventNameLevel " +
				            "ON J2.event_id = EventNameLevel.event_id) AS J3 " +
				            "WHERE J3." + sColumn + " LIKE '%" + sValue + "%'";

				icTemp = sqldTemp.RawQuery (sSQLQuery, null);
				if (!(icTemp != null)) {
					sMessage = "Record not found.";
				}
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
			return icTemp;
		}

		public Android.Database.ICursor GetRecordCursor (string sValue1, string sValue2, string sValue3 )
		{
			Android.Database.ICursor icTemp = null;
			try {
				sSQLQuery = "SELECT _id, name, mapName, level FROM (" +
				            "SELECT  EventList.event_id, mapName " +
				            "FROM EventList JOIN MapName " +
				            "ON EventList.map_id = MapName.map_id " +
				            "WHERE mapName LIKE " + "'%" + sValue1 + "%') AS J1 JOIN EventNameLevel " +
				            "ON J1.event_id = EventNameLevel.event_id " +
				            "WHERE name LIKE '%" + sValue2 + "%' AND level LIKE '" + sValue3 + "';";

	            icTemp = sqldTemp.RawQuery (sSQLQuery, null);

				if (!(icTemp != null)) {
					sMessage = "Record not found.";
				}
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
			return icTemp;
		}

		public Android.Database.ICursor GetMapNameCursor()
		{
			Android.Database.ICursor icTemp = null;
			try {
				sSQLQuery = "SELECT mapName FROM MapName";

				icTemp = sqldTemp.RawQuery (sSQLQuery, null);
				if (!(icTemp != null)) {
					sMessage = "Record not found.";
				}
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
			return icTemp;
		}
	

		~MyDatabase ()
		{
			try {
				sMessage = "";
				bDBIsAvailable = false;        
				sqldTemp.Close ();        
			} catch (SQLiteException ex) {
				sMessage = ex.Message;
			}
		}
	}
}


