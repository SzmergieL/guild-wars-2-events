using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Net;
using System.IO;
using System.Collections;
using Android.Database;
using System.Collections.Generic;
using Java.Lang;

namespace GuildWars2Events
{
	[Activity (Label = "Filter Events")]
	public class FilterActivity : Activity
	{
		MyDatabase mdTemp;
		EditText nameFilter, levelFilter;
		Spinner mapNameFilter;
		ImageView filterButton;
	
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			mdTemp = new MyDatabase ("events_db");

			SetContentView (Resource.Layout.DatabaseView);

			mapNameFilter = FindViewById<Spinner> (Resource.Id.MapNameSpinner);
//			mapNameFilter.Focusable = true;
//			mapNameFilter.FocusableInTouchMode = true;
//			mapNameFilter.RequestFocus ();

			nameFilter = FindViewById<EditText> (Resource.Id.nameFilter);
			nameFilter.ClearFocus ();
//			nameFilter.Focusable = false;
//			nameFilter.FocusableInTouchMode = false;

			levelFilter = FindViewById<EditText> (Resource.Id.levelFilter);
			levelFilter.ClearFocus ();
//			levelFilter.Focusable = false;
//			levelFilter.FocusableInTouchMode = false;

			filterButton = FindViewById<ImageView> (Resource.Id.filterButton);
			filterButton.Focusable = true;
			filterButton.FocusableInTouchMode = true;
			filterButton.RequestFocus ();


			ListView lvTemp = FindViewById<ListView> (Resource.Id.listView1);        
			lvTemp.ItemClick += new EventHandler<AdapterView.ItemClickEventArgs> (ListView_ItemClick);

			loadSpinnerData ();

			filterButton.Click += delegate {
				if ( ! mapNameFilter.Adapter.IsEmpty )
				{
					if ( levelFilter.Text.Trim () == "" && nameFilter.Text.Trim () == "" )
					{
						string sColumnName = "mapName";
						GetCursorView(sColumnName, mapNameFilter.SelectedItem.ToString().Trim ().Replace("'","''"));
					}
					else if (levelFilter.Text.Trim () == "") {
						GetCursorView (mapNameFilter.SelectedItem.ToString().Trim ().Replace("'","''"),nameFilter.Text.Trim (),"%");
					}
					else
					{
						GetCursorView (mapNameFilter.SelectedItem.ToString().Trim ().Replace("'","''"),nameFilter.Text.Trim (),levelFilter.Text.Trim ());
					}
				}
			};
//			filterButton.PerformClick ();
		}

		void ListView_ItemClick (object sender, AdapterView.ItemClickEventArgs e)
		{
			TextView nameShow = e.View.FindViewById<TextView> (Resource.Id.DIVname);
			TextView mapNameShow = e.View.FindViewById<TextView> (Resource.Id.DIVmapName);
			TextView levelShow = e.View.FindViewById<TextView> (Resource.Id.DIVlevel);
			var event_id = mdTemp.getEventId (nameShow.Text.ToString());
			mdTemp.AddFavRecord (event_id, nameShow.Text.ToString (), mapNameShow.Text.ToString (), Integer.ParseInt (levelShow.Text.ToString ()), "");
			Toast.MakeText (this, "Event added to favourites" , ToastLength.Long).Show();
		}

		private void loadSpinnerData() {
			List <System.String> lables = getAllLabels();
			ArrayAdapter<System.String> dataAdapter = new ArrayAdapter<System.String> (this, Android.Resource.Layout.SimpleSpinnerItem, getAllLabels());
			dataAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);

			if (dataAdapter.IsEmpty) {
				AlertDialog.Builder ad = new AlertDialog.Builder (this);
				ad.SetTitle ("Database is empty");
				ad.SetMessage ("Go to settings and sync Database");
				ad.SetPositiveButton ("Settings", delegate {
					StartActivity (typeof(SettingsActivity));
				});
				ad.SetNegativeButton ("Ignore", delegate {
				});
				ad.Show ();
			}	
			mapNameFilter.Adapter = dataAdapter;
		}

		public List<string> getAllLabels() {
			List <string> labels = new List<string>();
			ICursor cursor = mdTemp.GetMapNameCursor ();

			if ( cursor.MoveToFirst () ) {
				do {
					labels.Add ( cursor.GetString(0) );
				} while (cursor.MoveToNext());
			}

			cursor.Close();
			labels.Sort ();
			return labels;
		}

		protected void GetCursorView ()
		{
			Android.Database.ICursor icTemp = mdTemp.GetRecordCursor ();
			if (icTemp != null) {
				icTemp.MoveToFirst ();
				ListView lvTemp = FindViewById<ListView> (Resource.Id.listView1);
				string[] from = new string[] {"name","mapName","level" };
				int[] to = new int[] {
					Resource.Id.DIVname,
					Resource.Id.DIVmapName,
					Resource.Id.DIVlevel
				};
				SimpleCursorAdapter scaTemp = new SimpleCursorAdapter (this, Resource.Layout.DatabaseItemView, icTemp, from, to);
				lvTemp.Adapter = scaTemp;
			} else {
//				tvMsg.Text = mdTemp.Message;
			}
		}

		protected void GetCursorView (string sColumn, string sValue)
		{
			Android.Database.ICursor icTemp = mdTemp.GetRecordCursor (sColumn, sValue);
			if (icTemp != null) {
				icTemp.MoveToFirst ();
				ListView lvTemp = FindViewById<ListView> (Resource.Id.listView1);
				string[] from = new string[] {"name","mapName","level" };
				int[] to = new int[] {
					Resource.Id.DIVname,
					Resource.Id.DIVmapName,
					Resource.Id.DIVlevel
				};
				SimpleCursorAdapter scaTemp = new SimpleCursorAdapter (this, Resource.Layout.DatabaseItemView, icTemp, from, to);
				lvTemp.Adapter = scaTemp;
			}
		}

		protected void GetCursorView (string sValue1, string sValue2, string sValue3 )
		{
			Android.Database.ICursor icTemp = mdTemp.GetRecordCursor (sValue1, sValue2, sValue3 ); 
			ListView lvTemp = FindViewById<ListView> (Resource.Id.listView1); 
			string[] from = new string[] {"name","mapName","level" }; 
			int[] to = new int[] {
				Resource.Id.DIVname,
				Resource.Id.DIVmapName,
				Resource.Id.DIVlevel
			}; 
			if (icTemp != null) { 
				icTemp.MoveToFirst (); 
				SimpleCursorAdapter scaTemp = new SimpleCursorAdapter (this, Resource.Layout.DatabaseItemView, icTemp, from, to); 
				lvTemp.Adapter = scaTemp; 
			}
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			var MenuItem1 = menu.Add ("Favourite Events");
			var MenuItem2 = menu.Add ("Settings");

			return base.OnCreateOptionsMenu (menu);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.TitleFormatted.ToString()) {
			case "Favourite Events":
				StartActivity (typeof(FavListActivity));
				break;
			case "Settings":
				StartActivity (typeof(SettingsActivity));
				break;
			}
			return base.OnOptionsItemSelected (item);
		}
	}
}


