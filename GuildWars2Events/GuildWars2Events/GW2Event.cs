using System;
using System.Net;
using System.IO;
using System.Json;
using Android.Database;
using Android.Database.Sqlite;

namespace GuildWars2Events
{
	public class GW2Event
	{
		public int world_id { set; get; }
		public int map_id { set; get; }
		public string event_id { set; get; }
		public string state { set; get; }

		public string name { set; get; }
		public string mapName { set; get; }
		public int level { set; get; }

		public GW2Event ()
		{
		}

		public GW2Event (string name, string mapName, int level )
		{
			this.name = name;
			this.mapName = mapName;
			this.level = level;
		}
	}
}

